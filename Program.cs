﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

// For finding numbers: @"((\d+[\/\d.]*|\d)|(\-\d+[\/\d.]*|\d))"
// For finding the operator: @"([%^*\-+/]|[%^*\-+/][\" \"])"

namespace SharpCalc
{
    internal static class Program
    {
        private const string NumberPattern = @"((\d+[\/\d.]*|\d)|(\-\d+[\/\d.]*|\d))"; // regex pattern for finding numbers
        private const string OperatorPattern = @"([%^*\-+/]|[%^*\-+/]["" ""])"; // regex pattern for finding the operators

        private static MatchCollection ParseNumbers(string usrInput)
        {
            var numbers = Regex.Matches(usrInput, NumberPattern);

            return numbers;
        }

        private static MatchCollection ParseOperators(string usrInput)
        {
            var operators = Regex.Matches(usrInput, OperatorPattern);

            return operators;
        }

        private static object[] CalculateMaths(string usrInput, MatchCollection numbers,
            MatchCollection operators)
        {
            var numbersArray = new List<float>();

            for (var i = 0; i < numbers.Count; i++)
                numbersArray.Add(float.Parse(numbers[i].Value));

            var result = (float)0.0;
            if (operators[0].Value == "*" || operators[0].Value == "/")
                result = 1;

            // ReSharper disable once HeapView.ObjectAllocation.Possible
            foreach (var opertr in operators)
            {
                // ReSharper disable once HeapView.ObjectAllocation.Possible
                foreach (var number in numbersArray)
                {
                    if (opertr.ToString() == "-" && number < 0)
                        result = MathsOperations.Add(result, number * -1);
                    if (opertr.ToString() == "+")
                    {
                        result = MathsOperations.Add(result, number);
                    }
                    if (opertr.ToString() == "-")
                    {
                        result = MathsOperations.Sub(result, number);
                    }
                }
            }
            // ReSharper disable once HeapView.BoxingAllocation
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var returnArr = new object[] { usrInput, result };
            return returnArr;
        }

        private static string PrintResult(string usrInput)
        {
            if (usrInput == string.Empty)
                return "No input";

            var numbers = ParseNumbers(usrInput);
            var operators = ParseOperators(usrInput);

            var result = CalculateMaths(usrInput, numbers, operators);

            var prntResult = $"{result[0]} = {result[1]}";

            return prntResult;
        }

        private static void InitialiseConsole() // Inititalises the console for the user
        {
            Console.Title = "SharpCalc";
        }

        public static void Main(string[] args)
        {
            InitialiseConsole();

            Console.WriteLine("Welcome to {0}!", Console.Title);
            Console.WriteLine();
            while (true)
            {
                Console.Write("> ");
                var statement = Console.ReadLine();
                var result = PrintResult(statement);
                Console.WriteLine(result);
            }
        }
    }
}