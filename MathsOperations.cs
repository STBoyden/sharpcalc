﻿namespace SharpCalc
{
    public static class MathsOperations
    {
        public static float Add(float num1, float num2)
        {
            return num1 + num2;
        }

        public static float Sub(float num1, float num2)
        {
            return num1 - num2;
        }
    }
}